/*
 * --------------------------------------------------------------------------------------------------------------------
 * Example sketch/program showing how to read new NUID from a PICC to serial.
 * --------------------------------------------------------------------------------------------------------------------
 * This is a MFRC522 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid
 * 
 * Example sketch/program showing how to the read data from a PICC (that is: a RFID Tag or Card) using a MFRC522 based RFID
 * Reader on the Arduino SPI interface.
 * 
 * When the Arduino and the MFRC522 module are connected (see the pin layout below), load this sketch into Arduino IDE
 * then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
 * you present a PICC (that is: a RFID Tag or Card) at reading distance of the MFRC522 Reader/PCD, the serial output
 * will show the type, and the NUID if a new card has been detected. Note: you may see "Timeout in communication" messages
 * when removing the PICC from reading distance too early.
 * 
 * @license Released into the public domain.
 * 
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 */

#include <SPI.h>
#include <MFRC522.h>
#include <painlessMesh.h>

// Setup RFID reader
#define SS_PIN 15
#define RST_PIN 0

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class

// Init array that will store new NUID 
byte nuidPICC[7];

// Setup Painless Mesh
#define   MESH_PREFIX     "abeuni-caravana-mesh"
#define   MESH_PASSWORD   "abeuni01"
#define   MESH_PORT       5555
#define   MESH_CHANNEL    6

void receivedCallback(uint32_t from, String &msg);
void newConnectionCallback(uint32_t nodeId);

Scheduler userScheduler;
painlessMesh  mesh;
uint32_t bridgeId = 0;

Task probeBridgeTask(10000, TASK_FOREVER, []() {
  Serial.println("Probing for bridge...");
  mesh.sendBroadcast("WHO IS BRIDGE");
});

// Setup
void setup() { 
  Serial.begin(115200);

  // Init RFID reader
  Serial.println("Starting RFID Reader...");
  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522
  Serial.println("Started RFID Reader!");
  
  // Init Painless Mesh
  Serial.println("Connecting to mesh...");
  mesh.setDebugMsgTypes(ERROR | STARTUP);
  mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, MESH_CHANNEL);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  userScheduler.addTask(probeBridgeTask);
}
 
void loop() {
  // Mesh things
  
  // Update Mesh and scheduler
  mesh.update();

  // If bridge is not reachable, or mesh is not connected do nothing
  if (!bridgeId || !mesh.isConnected(bridgeId)) {
    if (!probeBridgeTask.enableIfNot()) {
      Serial.println("Searching for bridge...");
      bridgeId = 0;
    }
    return;
  }
  
  // RFID things
  
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if (!rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been read
  if (!rfid.PICC_ReadCardSerial())
    return;

  // Check if same NUID as last read
  bool is_same = true;
  for (byte i = 0; i < 7; i++) {
    is_same = is_same && (nuidPICC[i] == rfid.uid.uidByte[i]);
  }

  // If is same card, just do nothing
  if (is_same)
    return;
    
  // Store NUID into nuidPICC array
  for (byte i = 0; i < 7; i++) {
    nuidPICC[i] = rfid.uid.uidByte[i];
  }

  String NUIDString = "";

  for (byte i = 0; i < 7; i++) {
    byte current = nuidPICC[i];
    if (current < 0x10) NUIDString += "0";
    NUIDString += String(current, HEX);
  }

  NUIDString.toUpperCase();

  Serial.print("Read card ");
  Serial.println(NUIDString);

  mesh.sendSingle(bridgeId, NUIDString);

  // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}

void receivedCallback(uint32_t from, String &msg) {
  if (msg == String("IS BRIDGE")) {
    Serial.print("Found bridge at ");
    Serial.print(from);
    Serial.println("!");

    bridgeId = from;
    probeBridgeTask.disable();
    
    return;
  }
  
  Serial.print("Unknown message format from ");
  Serial.print(from);
  Serial.println(":");
  Serial.println(msg);
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.print("Connected to ");
  Serial.println(nodeId);
}
