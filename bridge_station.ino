/*
 * --------------------------------------------------------------------------------------------------------------------
 * Example sketch/program showing how to read new NUID from a PICC to serial.
 * --------------------------------------------------------------------------------------------------------------------
 * This is a MFRC522 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid
 * 
 * Example sketch/program showing how to the read data from a PICC (that is: a RFID Tag or Card) using a MFRC522 based RFID
 * Reader on the Arduino SPI interface.
 * 
 * When the Arduino and the MFRC522 module are connected (see the pin layout below), load this sketch into Arduino IDE
 * then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
 * you present a PICC (that is: a RFID Tag or Card) at reading distance of the MFRC522 Reader/PCD, the serial output
 * will show the type, and the NUID if a new card has been detected. Note: you may see "Timeout in communication" messages
 * when removing the PICC from reading distance too early.
 * 
 * @license Released into the public domain.
 * 
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 */

#include <painlessMesh.h>
#include <ESP8266HTTPClient.h>

// Server address
#define   SERVER_URL      "http://10.10.15.135:3000/graphql"

// Wifi config
#define   EXTERNAL_SSID   "home hariki"
#define   EXTERNAL_PASS   "rose5265"

// Setup Painless Mesh
#define   MESH_PREFIX     "abeuni-caravana-mesh"
#define   MESH_PASSWORD   "abeuni01"
#define   MESH_PORT       5555
#define   MESH_CHANNEL    6

void receivedCallback(uint32_t from, String &msg);
void newConnectionCallback(uint32_t nodeId);

Scheduler userScheduler;
painlessMesh  mesh;

// Setup
void setup() { 
  Serial.begin(115200);
  
  // Init Painless Mesh
  Serial.println("Connecting to mesh...");
  mesh.setDebugMsgTypes(ERROR | STARTUP);
  mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, MESH_CHANNEL);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);

  // Connect to external AP
  Serial.println("Connecting to external AP...");
  Serial.print("MAC Address: ");
  Serial.println(WiFi.macAddress());
  mesh.stationManual(EXTERNAL_SSID, EXTERNAL_PASS);
}
 
void loop() {
  // Mesh things
  
  // Update Mesh and scheduler
  mesh.update();
}

void receivedCallback(uint32_t from, String &msg) {
  if (msg == String("WHO IS BRIDGE")) {
    Serial.print("Answering to bridge probe from ");
    Serial.print(from);
    Serial.println("!");

    mesh.sendSingle(from, "IS BRIDGE");
    
    return;
  }

  if (msg.length() == 14) {
    Serial.print("Card with NUID ");
    Serial.print(msg);
    Serial.print(" read at ");
    Serial.println(from);

    // Send request
    if (WiFi.status() == WL_CONNECTED) {
      
      WiFiClient wifi;
      HTTPClient http;
      String payload;
      
      if (http.begin(wifi, SERVER_URL)) {
        http.addHeader("Content-Type", "application/json");

        DynamicJsonDocument doc(2048);

        doc["operationName"] = "scanned";
        doc["query"] = "mutation scanned($nuid: String, $node: Int!) { scannedCard(nuid: $nuid, node: $node) { id } }";
        JsonObject variables = doc.createNestedObject("variables");
        variables["node"] = from;
        variables["nuid"] = msg;
        
        serializeJson(doc, payload);
        
        int response = http.POST(payload);
      } else Serial.println("Couldn't connect to server!");
      
    } else Serial.println("WiFi is not connected!");

    return;
  }
  
  Serial.print("Unknown message format from ");
  Serial.print(from);
  Serial.println(":");
  Serial.println(msg);
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.print("Connected to ");
  Serial.println(nodeId);
}
